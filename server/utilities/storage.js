import multer from 'multer';
import crypto from 'crypto';
import mime from 'mime';


import config from '../config';


let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config.imagePath)
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, 'resize-' + raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
        });
    }
});

let  upload =  multer({ storage });

export default upload;
