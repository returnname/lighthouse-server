import express from 'express';

import Video from '../models/video';

import url from 'url';
import db from '../utilities/database';

let router = express.Router();

router.get('/', async (req,res,next) => {
    let videos;

    try {
        videos = await Video.find({});
    }catch (err){

        next(err);
    }

    res.render('pages/videos', { videos });
});

router.delete('/', async  (req,res,next) => {
    let id = +req.body.id;

    try{
        await Video.findOneAndRemove({videoId : id});
    }catch (err){
        next(err);
    }

    res.json({success:true});
});



router.get('/new', (req,res) => {

    res.render('pages/newVideo');
});
router.post('/new',  async ( req,res,next ) => {
    let link = url.parse(req.body.video);

    try {
        await Video.create({ vimeoId: +link.path.substring(1) });
    }catch (err){
        next(err);
    }

    res.redirect('back');
});


router.get('/*', (req,res) => {


    res.redirect('/');
});

export default router;