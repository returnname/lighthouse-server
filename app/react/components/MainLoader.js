import React, {Component} from 'react';

import { ClipLoader } from 'react-spinners';


const style ={
    width:'100%',
    height:'100%',
    display:'flex',
    alignItems:'center',
    justifyContent:'center'
};

export default class MainLoader extends Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }


    render(){
        return(
            <div style={style}>
                <ClipLoader
                    color={'#e9c4b0'}
                    loading={this.state.loading}
                />
            </div>
        );
    }
}