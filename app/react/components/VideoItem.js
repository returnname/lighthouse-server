import React, {Component} from 'react';



export default class VideoItem extends Component{



    render(){
        return(
            <div className="video-item">
                <div className="video-item__header">

                </div>
                <div className="responsive-video">
                    <iframe src={"https://player.vimeo.com/video/" + this.props.vimeoId}
                            frameBorder="0"
                            allowFullScreen
                    />
                </div>
            </div>
        );
    }

}