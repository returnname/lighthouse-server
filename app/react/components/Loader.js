import React, {Component} from 'react';

import { ClipLoader } from 'react-spinners';

export default class Loader extends Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }


    render(){
        return(
            <div className="loading-container">
                <ClipLoader
                    color={'#e9c4b0'}
                    loading={this.state.loading}
                />
            </div>
        );
    }
}