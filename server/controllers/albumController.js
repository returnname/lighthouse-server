import express from 'express';

import imageResize from '../utilities/image';
import upload from '../utilities/storage';

import Album from '../models/album';

let router = express.Router();

router.get('/', async (req , res , next) => {
    let albums;

    try{
        albums = await Album.find({ isInfo:false });
    }catch (err){
        next(err);
    }


    res.render('pages/albums', { albums });
});

router.get('/new', (req,res) => {

    res.render('pages/newAlbum');
});

router.get('/edit', (req,res) => {

    res.render('pages/newAlbum');
});

router.post('/new', upload.fields([{name: 'cover' , maxCount:1 } , {name: 'images' , maxCount:100 } ]) , async (req,res,next) => {
    let {man ,woman} = req.body;
    let cover = req.files.cover[0].filename;
    let images = req.files.images.map(image=>{
       return image.filename;
    });

    if (cover && images){
        try {
            await Album.create({ man, woman, cover, images });
        } catch ({ message }) {
            next(message);
        }
    }

    res.redirect('back');
});

router.get('/:id/edit', async (req,res,next)=>{
    let albumId = +req.params.id;

    let album;
    try {
       album = await Album.findOne({ albumId });
    }catch (err){
        next(err);
    }

    res.render('pages/editAlbum',{ data:album});
});

router.post('/:id/update', async (req,res,next)=>{
    let id = req.params.id,
        {man ,woman} = req.body;

    try {
        await Album.findOneAndUpdate( {albumId:id},{ man,woman } );
        res.redirect('/admin/albums');
    }catch (err){
        next(err);
    }

});

router.get('/:id', async (req,res,next) => {
    let id = req.params.id;
    let album;

    try {
        album =  await Album.findOne( {albumId:id} );
        res.render('pages/fullAlbum', { album });
    }catch (err){
        next(err);
    }

});

router.delete('/' , async (req,res,next)=>{
    let id = req.body.id;

    let data;

    try {
        data = await Album.findOne({ albumId:id });
        if (data) data.remove();
    }catch (error){
        next(error);
    }

    res.json({ success:true });
});

router.get('/*', (req,res) => {

    res.send('all albums');
});

export default router;