import React,{Component} from 'react';

import HeaderComponent from './Header';
import MainContainer from '../containers/MainContainer'
import FooterComponent from './Footer';

import MainLoader from '../components/MainLoader';

export default class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            loading : true
        };

        this.handleLoad = this.handleLoad.bind(this);
    }

    componentDidMount() {
        window.addEventListener('load', this.handleLoad);
    }

    handleLoad() {
        this.setState({
            loading : !this.state.loading
        })
    }


    render(){
        if (this.state.loading){

            return <MainLoader/>
        }

        return (

            <div>
                <HeaderComponent/>
                <MainContainer/>
                <FooterComponent/>
            </div>
        )
    }

}



