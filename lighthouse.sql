-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 12 2017 г., 17:13
-- Версия сервера: 5.6.37
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `lighthouse`
--

-- --------------------------------------------------------

--
-- Структура таблицы `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `man` varchar(255) DEFAULT NULL,
  `woman` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `album`
--

INSERT INTO `album` (`id`, `man`, `woman`, `cover`) VALUES
(6, 'Антон', 'Бабонька', 'd25ed8ac3991275f774954fead5841021510319102651.jpeg'),
(7, 'Валентин', 'Дядька', '03c295be865b5651e4426414ae675e921510319356372.jpeg'),
(8, 'Слава', 'КПСС', '503c8f804d0d7f7e1fd6a91e204121031510319382323.jpeg'),
(9, 'Бутер', 'Бродский', 'cde3f2162931f3a8e5255f616fa6448f1510319584635.jpeg'),
(10, 'Гнойный', 'Оксимарон', '17c56b3f829424c9af908708d73952b01510319609707.jpeg'),
(11, 'Валерий', 'Клава', '20ea8724b432cb7fa057f75d71d980f21510319658317.jpeg'),
(12, 'Леха', 'Настюха', '1ecf03424afc40078badf7f3522bb10d1510319676309.jpeg');

-- --------------------------------------------------------

--
-- Структура таблицы `album_images`
--

CREATE TABLE `album_images` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `album_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `album_images`
--

INSERT INTO `album_images` (`id`, `image`, `album_id`) VALUES
(10, 'fbf6c1dca9204b2c86505c040edf89181510319102664.jpeg', 6),
(11, '7d947c07885cabc139a629d931bb86d11510319102672.jpeg', 6),
(12, 'ecb0f59184647debbec2de6e71f8ea701510319356376.jpeg', 7),
(13, '058399dc77519674502e90b8899cff8b1510319356386.jpeg', 7),
(14, '02c64537f8e1c6787cfcd21a0c4634ba1510319382334.jpeg', 8),
(15, 'ef4e4abeeb2f6656765a94b0c0a374891510319382340.jpeg', 8),
(16, 'df58c5a7f7271fc3efb259f29ef358631510319584637.jpeg', 9),
(17, '57c86889f26f0802e5f84a17f52d6b901510319584637.jpeg', 9),
(18, '4130a91a16c73bef45f8404a58d42ea91510319584638.jpeg', 9),
(19, '0ef50eaabbaca51d9eab4de22d0aa25a1510319609709.jpeg', 10),
(20, '148b0c45ec0aee4ddf4cc18b8b6de3081510319609709.jpeg', 10),
(21, '825a8aeabded5695bb888a90768197c61510319609712.jpeg', 10),
(22, 'ec87e73c773df7c2d0c52ca8ee1b2c8a1510319658318.jpeg', 11),
(23, '35e7faafbd598b06773a93dd50ff79d61510319658319.jpeg', 11),
(24, '71d8a2f0bdce0fd81abdff9d0152f2751510319676310.jpeg', 12),
(25, '87f62d9bb72da3642ef2e002be26739b1510319676314.jpeg', 12);

-- --------------------------------------------------------

--
-- Структура таблицы `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `vimeo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `video`
--

INSERT INTO `video` (`id`, `vimeo_id`) VALUES
(3, 190901087);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `album_images`
--
ALTER TABLE `album_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `album_images`
--
ALTER TABLE `album_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
