import React,{Component} from 'react';

import axios from 'axios';
import DocumentTitle from 'react-document-title';


import AlbumFull from '../components/AlbumFull';

import Loader from '../components/Loader';
import Error from '../components/Error';



class AlbumContainer extends Component {
    constructor(props){
        super(props);

        console.log('constructor');
        this.state = {
            albumId: this.props.match.params.id,
            loading: true,
            error:false
        }
    }
    componentDidMount(){
        console.log('willMount');
        axios.get('/api/album/' + this.state.albumId )
            .then((response) =>{
                console.log(response);
                this.setState({
                    album: response.data,
                    loading:!this.state.loading
                });
            })
            .catch(() => {
                this.setState({
                    error: !this.state.error
                });
            });
    }
    render(){
        if (this.state.loading){
            if (this.state.error){
                return <Error/>
            }

            return <Loader/>
        }

        return (
            <DocumentTitle title={this.state.album.man + ' и ' + this.state.album.woman + '| «LightHouse» - свадебная съемка во Владимире'}>
                <AlbumFull album={this.state.album} />
            </DocumentTitle>
        );
    }

}export default AlbumContainer;









