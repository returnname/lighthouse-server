import React,{Component} from 'react';

import {Switch , Route} from 'react-router-dom';


import Home from '../components/HomeGrid';
import Contacts from '../components/Contacts';
import Info from '../components/Info';
import Photo from './PhotoContainer';
import Video from './VideoContainer';
import Album from './AlbumContainer';


class MainContainer extends Component {
    render(){
        return (
                <main>
                    <div className="wrapper">
                        <Switch>
                            <Route exact path='/' component={Home}/>
                            <Route exact path='/album' component={Photo}/>
                            <Route  path='/album/:id' component={Album}/>
                            <Route  path='/contacts' component={Contacts}/>
                            <Route  path='/info' component={Info}/>
                            <Route  path='/video' component={Video}/>
                        </Switch>
                    </div>
                </main>
        )
    }

}export default MainContainer;




