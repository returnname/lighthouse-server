import express from 'express';

import db from '../utilities/database';
import upload from '../utilities/storage';

let router = express.Router();

import Album from '../models/album';

const configForm = [
    {name: 'image' , maxCount:3}
];


router.get('/', (req,res) => {

    res.render('pages/options');
});

router.post('/', upload.fields(configForm) , async (req,res,next) => {

    let images = req.files.image.map(image=>{
        return image.filename;
    });

    try {
        await Album.findOne({ isInfo:true }).remove();
        await Album.create({ images , isInfo:true });
    }catch (err){
        next(err);
    }

    res.render('pages/options');
});

export default router;