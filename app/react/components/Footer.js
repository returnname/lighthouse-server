import React,{Component} from 'react';


class Footer extends Component {
    render(){
        return (
            <footer>
                <div className="social-container">
                    <a className="social-container__item" href={'https://vimeo.com/lighthousevideos'}>
                        <img src="/icons/vimeo.png" alt=""/>
                    </a>
                    <a className="social-container__item" href={'https://vk.com/lighthouse_group'}>
                        <img src="/icons/vk.png" alt=""/>
                    </a>
                    <a className="social-container__item" href={'https://www.instagram.com/denis_averyanov/'}>
                        <img src="/icons/instagram.png" alt=""/>
                    </a>
                </div>
            </footer>
        )
    }

}export default Footer;