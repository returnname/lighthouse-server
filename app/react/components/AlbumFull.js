import React,{Component} from 'react';


export default class AlbumFull extends Component{

    render(){
        const images  = this.props.album.images.map((data,key)=>{
            return <img src={ '/images/' + data } className="responsive-image" key={key}/>
        });

        return(
            <div className="album-container">
                <div className="album-header">
                    {this.props.album.man + ' и ' + this.props.album.woman}
                </div>
                <div className="album-content">
                    {images}
                </div>
            </div>
        )
    }

}