import React,{Component} from 'react';

import axios from 'axios';

import DocumentTitle from 'react-document-title';

import Loader from '../components/Loader';
import Error from '../components/Error';


class Info extends Component {
    constructor(){
        super();

        this.state = {
            photos:[],
            loading : true,
            error: false
        };
    }
    componentDidMount(){
        axios
            .get('/api/info')
            .then(photos=>{
                console.log(photos.data);
                return this.setState({
                    photos : photos.data,
                    loading : !this.state.loading
                });
            })
            .catch(() => {
                this.setState({
                    error: !this.state.error
                });
            });
    }

    handleError(){
        this.setState({
            photos :
                [
                'https://pp.userapi.com/c639921/v639921373/62a57/9mw3M9e0CiQ.jpg',
                'https://pp.userapi.com/c639921/v639921373/62a57/9mw3M9e0CiQ.jpg',
                'https://pp.userapi.com/c639921/v639921373/62a57/9mw3M9e0CiQ.jpg'
                ],
            loading : !this.state.loading,
            error: false
        });
    }

    render(){
        if (this.state.loading){
            if (this.state.error){
                this.handleError();
            }

            return <Loader/>
        }
        return (
            <DocumentTitle title={' Услуги | «LightHouse» - свадебная съемка во Владимире'}>
            <div className="price-container">
                <div className="price-item">
                    <img src={'/images/' + this.state.photos[0]} className="price-item__image"/>
                        <div className="price-item__header">

                            <span className="name"> Пакет I</span>
                            <span className="price-item__header__cost"> 42 000 р.-</span>
                        </div>
                        <ul className="price-item__list">
                            Фото и видео съемка в течение 6 часов
                            <li className="price-item__list__item">не менее 150 фотографий</li>
                            <li className="price-item__list__item">полная консультация по свадебному дню</li>
                            <li className="price-item__list__item">свадебный фильм</li>
                            <li className="price-item__list__item">свадебный клип</li>
                            <li className="price-item__list__item">цветокоррекция фотографий, ретушь крупных планов</li>
                            <li className="price-item__list__item">запись на индивидуально оформленную флешку</li>
                        </ul>

                </div>
                <div className="price-item">
                    <img src={'/images/' + this.state.photos[1]} className="price-item__image"/>
                        <div className="price-item__header">

                            <span className="name"> Пакет II</span>
                            <span className="price-item__header__cost"> 65 000 р.-</span>
                        </div>
                        <ul className="price-item__list">
                            Фото и видео съемка в течение 10 часов
                            <li className="price-item__list__item">не менее 300 фотографий</li>
                            <li className="price-item__list__item">полная консультация по свадебному дню</li>
                            <li className="price-item__list__item">свадебный фильм</li>
                            <li className="price-item__list__item">свадебный клип</li>
                            <li className="price-item__list__item">цветокоррекция фотографий, ретушь крупных планов</li>
                            <li className="price-item__list__item">запись на индивидуально оформленную флешку</li>
                        </ul>

                </div>
                <div className="price-item">
                    <img src={'/images/' + this.state.photos[2]} className="price-item__image"/>
                        <div className="price-item__header">

                            <span className="name">Пакет III</span>
                            <span className="price-item__header__cost">90 000 р.-</span>
                        </div>
                        <ul className="price-item__list">
                            Фото и видео съемка в течение 12 часов
                            <li className="price-item__list__item">не менее 400 фотографий</li>
                            <li className="price-item__list__item">полная консультация по свадебному дню</li>
                            <li className="price-item__list__item">свадебный фильм</li>
                            <li className="price-item__list__item">свадебный клип</li>
                            <li className="price-item__list__item">предсвадебная съемка (Lovestory)</li>
                            <li className="price-item__list__item">цветокоррекция фотографий, ретушь крупных планов</li>
                            <li className="price-item__list__item">печать 20 фотографий</li>
                            <li className="price-item__list__item">10 готовых фото в течении 3х дней после свадьбы</li>
                            <li className="price-item__list__item">фотокнига в твердом переплете</li>
                            <li className="price-item__list__item">запись на индивидуально оформленную флешку</li>
                        </ul>

                </div>
                <div>

                </div>
            </div>
            </DocumentTitle>
        )
    }
}export default Info;




