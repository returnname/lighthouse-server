import React,{Component} from 'react';

import DocumentTitle from 'react-document-title';


class Contacts extends Component {
    render(){
        return (
            <DocumentTitle title={' Контакты | «LightHouse» - свадебная съемка во Владимире'}>
            <div className="contacts-container">
                <div className="contacts-container__image">
                    <img src="https://pp.userapi.com/c626629/v626629566/58d59/TKchzqwamBU.jpg" className="responsive-image" style={{maxWidth:'275px'}}/>
                </div>
                <div className="contacts-container__text">
                    <h2>Наши контакты</h2>
                    <p>
                        <span>Телефон/WhatsApp:</span> +7(930)0328918 +7(910)6730518
                    </p>
                    <p>
                        <span>E-mail:</span> averyanovdenis33@gmail.com
                    </p>
                    <p>
                        <span>Instagram:</span> <a href="https://www.instagram.com/denis_averyanov/">@denis_averyanov</a>
                    </p>
                </div>
            </div>
            </DocumentTitle>
        )
    }

}export default Contacts;




