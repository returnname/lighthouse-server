export default(req,res,next) => {

    if (!req.session.authorized && !(req.url === '/login')){
        console.log('redirected');
        res.redirect('/admin/login');
        return;
    }



    next();
}