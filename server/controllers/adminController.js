import express from 'express';

import albumController from '../controllers/albumController';
import optionsController from '../controllers/optionsController';
import videoController from '../controllers/videoController';




import auth from '../middlewares/auth';
import login from '../middlewares/login';
import logout from '../middlewares/logout';


let router = express.Router();

router.use(auth);


router.post('/login' , login , (req,res) => {
    res.redirect('/admin/albums');
});

router.get('/login', (req,res) => {
    res.render('login');
});

router.all('/logout', logout ,(req,res) => {
    res.render('login');
});

router.get('/dashboard' , (req,res) => {
    res.render('pages/dashboard');
});


router.use('/albums' , albumController);
router.use('/video' , videoController);
router.use('/options' , optionsController);

export default router;