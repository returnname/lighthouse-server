import mongoose, { Schema } from 'mongoose';
import counter from '../models/counter';


const VideoSchema = new Schema({
    videoId:{ type: Number , default: 0 },
    vimeoId:{type: String}
});

VideoSchema.pre('save', async function (next){
    try{
        let count = await counter.findOneAndUpdate({schemaName: 'video'}, {$inc: { seq: 1} });
        this.videoId = count.seq;
    } catch (message){
        next(message);
    }
    next();
});

export default mongoose.model('Video', VideoSchema);