import mongoose, { Schema } from 'mongoose';

import counter from '../models/counter';

import fs from 'fs';

import config from '../config';


const AlbumSchema = new Schema({
    albumId:{ type: Number , default: 0 },
    man: { type: String },
    woman :{ type:String },
    images : [],
    cover :{ type : String },
    isInfo: {type: Boolean , default: false}
});

AlbumSchema.pre('save', async function (next){
    try{
        let count = await counter.findOneAndUpdate({schemaName: 'album'}, {$inc: { seq: 1} });
        this.albumId = count.seq;
    } catch (message){
        next(message);
    }

    next();
});



AlbumSchema.pre('remove' , async function (next) {

    fs.unlinkSync(config.imagePath + '/' + this.cover);

    this.images.forEach(result =>{
        fs.unlinkSync(config.imagePath  + '/' +  result);
    });

    next();
});


export default mongoose.model('Album', AlbumSchema);