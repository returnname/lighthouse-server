import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import browserSync from 'browser-sync';
import cssnano from 'gulp-cssnano';
import del from 'del';
import rename from 'gulp-rename';

import browserify from 'browserify';
import babelify from 'babelify';
import gutil from 'gulp-util';
import source from 'vinyl-source-stream';

import uglify from 'gulp-uglify';
import streamify from 'gulp-streamify';

import sftp from 'gulp-sftp';



gulp.task('sass', () => {
    gulp.src('./app/sass/**/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./app/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('clean' , () => {
    del.sync('server/public/css');
    del.sync('server/public/*.js');
});


gulp.task('react',()=>{
     browserify({
        entries: './app/react/app.js',
        extensions: ['.jsx'],
        debug: true
    })
         .transform('uglifyify', { global: true  })
         .transform('babelify', {
            presets: ['es2015', 'react']
        })
         .bundle()
         .on('error', function(err){
            gutil.log(gutil.colors.red.bold('[browserify error]'));
            gutil.log(err.message);
            this.emit('end');
        })
         .pipe(source('bundle.js'))
         .pipe(streamify(uglify()))
         .pipe(gulp.dest('app'))
         .pipe(browserSync.reload({
             stream: true
         }));

});

gulp.task('browser-sync',() => {


    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'app' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});

gulp.task('watch' ,['browser-sync','react'] ,() => {
    gulp.watch('app/**/**/*.sass', ['sass']);
    gulp.watch('app/react/**/*.js', ['react','build']); // React task
    gulp.watch('app/*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
    gulp.watch('app/js/**/*.js', browserSync.reload);   // Наблюдение за JS файлами в папке js
});


gulp.task('build',['clean'],() => {

    gulp.src('app/css/**.css')
        .pipe(cssnano())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest('assets/css'));

    gulp.src('app/**.js')
        .pipe(gulp.dest('assets/'));
});




gulp.task('default', ['watch']);