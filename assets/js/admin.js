$(document).ready(function () {

    $("button#delete").on('click',function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id'),
            type = $(this).attr('data-type');


        console.log(id, type);


        $.ajax({
            url:"/admin/"+ type ,
            type:"DELETE",
            data:{ id:id },
            success: function (json) {
                if(json.success){
                    location.reload();
                }
            }
        });


    });
});