import path from 'path';

import im from 'imagemagick';

import config from '../config';

import fs from 'fs';


export default (req , res , next)=>{
    let cover = req.files.cover[0].filename;
    let images = req.files.images;

    images.forEach((image)=>{
        im.resize({
            srcData: fs.readFileSync(config.imagePath + "/" + image.filename, 'binary'),
            width:   900
        }, function(err, stdout, stderr){
            if (err) throw err + stderr;
            fs.writeFileSync(path.resolve(config.imagePath,'resize-' + image.filename), stdout, 'binary');
            //fs.unlinkSync(path.resolve(config.imagePath, image.filename));
            console.log('resize image');
        });
    });

    im.resize({
        srcData: fs.readFileSync(config.imagePath + "/" + cover, 'binary'),
        width:   900
    }, function(err, stdout, stderr){
        if (stderr) throw err + stderr;
        fs.writeFileSync(path.resolve(config.imagePath,'resize-' + cover), stdout, 'binary');
        //fs.unlinkSync(path.resolve(config.imagePath, cover));
        console.log('resize cover');
    });

    next();
}