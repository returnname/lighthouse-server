import React,{Component} from 'react';

import DocumentTitle from 'react-document-title';
import axios from 'axios';

import PhotoItem from '../components/PhotoItem';



import Loader from '../components/Loader';
import Error from '../components/Error';

class PhotoContainer extends Component {
    constructor(){
        super();


        this.state = {
            photos:[],
            loading:true,
            error:false
        };
    }

    componentDidMount(){
        axios
            .get('/api/albums/all')
            .then(albums=>{
                console.log(albums.data);
                return this.setState({
                photos : albums.data,
                loading: !this.state.loading
                });
            })
            .catch(() => {
                this.setState({
                    error: !this.state.error
                });
            });
    }
    render(){
        if (this.state.loading){
            if (this.state.error){
                return <Error/>
            }
            return <Loader/>
        }

        const ImagesBlocks = this.state.photos.map((photo , key) =>{
            let { man ,woman , cover , albumId} = photo;

            return <PhotoItem  man={man}  woman={woman} cover={cover} key={key} id={albumId}/>
            }
        );

        return (
            <DocumentTitle title={'Альбомы | «LightHouse» - свадебная съемка во Владимире'}>
                <div className="grid-main">
                    {ImagesBlocks}
                </div>
            </DocumentTitle>
        )
    }
}export default PhotoContainer;




