import mongoose, { Schema } from 'mongoose';

const CounterSchema = new Schema({
    schemaName:{ type : String },
    seq : {type: Number , default: 1 }
});


export default mongoose.model('Counter' , CounterSchema);


