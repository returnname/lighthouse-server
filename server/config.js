import 'dotenv/config';


const config = {
    db:{
        connectionLimit : 10,
        host     : process.env.DB_HOST,
        user     : process.env.DB_USER,
        password : process.env.DB_PASS,
        database : 'lighthouse'
    },
    mongo:{
        uri:'mongodb://localhost/lighthouse',
        options: { useMongoClient: true }
    },
    publicPath:'assets/',
    imagePath: 'assets/images'

};
export default config;
