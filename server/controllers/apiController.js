import express from 'express';

import Album from '../models/album';
import Video from '../models/video';

let router = express.Router();

router.get('/info', async ( req , res ,next ) => {
    let album;

    try{
        album = await Album.findOne({isInfo:true});
    } catch (err){
        next(err)
    }

    res.json(album.images);
});

router.get('/albums/:qty', async ( req , res ,next ) => {
    let qty = +req.params.qty;

    let albums;

    try{
        albums = await Album.find({isInfo:false})
            .limit(qty);
    } catch (err){
        next(err)
    }

    res.json(albums);
});

router.get('/album/:id', async (req,res,next) => {
    let id = req.params.id;
    let album;

    try{
        album = await Album.findOne({ albumId: id });
    }catch (err){
        next(err);
    }


    res.json(album);
});

router.get('/videos/:qty', async ( req,res,next ) => {
    let qty = +req.params.qty;

    let videos ;


    try{
        videos = await Video.find({})
            .limit(qty);
    }catch (err){
        next(err);
    }

    res.json(videos);
});


export default router;