import React,{Component} from 'react';

import axios from 'axios';
import DocumentTitle from 'react-document-title';

import VideoItem from '../components/VideoItem';

import Loader from '../components/Loader';
import Error from '../components/Error';

class VideoContainer extends Component {
    constructor(){
        super()

        this.state = {
            videos :[],
            loading: true,
            error:false
        };

    }

    componentWillMount(){
        axios
            .get('/api/videos/all')
            .then(response=>{
                this.setState({
                    videos: response.data,
                    loading: !this.state.loading
                });
            })
            .catch(() => {
                this.setState({
                    error: !this.state.error
                });
            });
    }


    render(){
        if (this.state.loading){
            if (this.state.error){
                return <Error/>
            }

            return <Loader/>
        }
        const videoContent = this.state.videos.map((video,key)=>{
           return <VideoItem vimeoId={video.vimeoId} key={key}/>
        });

        return (
            <DocumentTitle title={'Видео | «LightHouse» - свадебная съемка во Владимире'}>
                <div className="video-container">
                    {videoContent}
                </div>
            </DocumentTitle>
        )
    }

}export default VideoContainer;




