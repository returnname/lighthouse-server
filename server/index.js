import express from 'express'

import bodyParser from 'body-parser';
import session from 'express-session';


import apiController from './controllers/apiController';
import adminController from './controllers/adminController';


import config from './config';

import bluebird from 'bluebird';
import mongoose from 'mongoose';

//Start application
let  app = express();


app.set('trust proxy', 1);
app.set('views', 'server/views');
app.set('view engine', 'pug');


//DB
mongoose.Promise = bluebird;
mongoose.connect(config.mongo.uri, config.mongo.options , err => {
    if (err) throw err;

    console.log(`Mongo connected!`);
});

app.use(express.static( config.publicPath ));


app.use(session({
    secret: 'pass',
    resave: false,
    saveUninitialized: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/api' , apiController);
app.use('/admin' , adminController);

app.get('*', (req, res) => {

    res.render('main');
});

console.log(app.get('env'));

app.listen(3000, () => console.log('Running on localhost:3000'));