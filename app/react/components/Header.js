import React,{Component} from 'react';

import {Link} from 'react-router-dom';



class Header extends Component {
    render(){
        return (
            <header>
                <div className="wrapper">
                    <div className="logo">
                        <Link className="menu-item"  to={"/"}>LightHouse</Link>
                    </div>
                    <div className="menu">
                        <Link className="menu-item" to={"/album"}>Photo</Link>
                        <Link className="menu-item" to={'/video'}>Video</Link>
                        <Link className="menu-item" to={'/info'}>Info</Link>
                        <Link className="menu-item" to={'/contacts'}>Contact</Link>
                    </div>
                </div>
            </header>
        )
    }

}export default Header;




