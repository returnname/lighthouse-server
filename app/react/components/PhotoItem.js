import React,{Component} from 'react';

import {Link} from 'react-router-dom';





class PhotoItem extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="grid-item">
                <Link to={'/album/' + this.props.id}>
                    <figure className="effect-chico">
                        <img src={'/images/' + this.props.cover} className="responsive-image"/>
                        <figcaption>
                            <h2>{this.props.man} <span>{this.props.woman}</span></h2>
                        </figcaption>
                    </figure>
                </Link>
            </div>
        )
    }

}export default PhotoItem;






